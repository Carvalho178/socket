#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORTA 5050
#define TAM 4096
#define SERVER_ADDR "127.0.0.1"

void mostrarmenu2(){
        system("clear");
        printf("------------------------\n");
        printf("          Nome          \n");
        printf("        Telefone        \n");
        printf("------------------------\n");
}

void mostrarmenu(){
        system("clear");
        printf("------------------------\n");
        printf("   add - Add Contato    \n");
        printf(" prc - Procurar contato \n");
        printf("    tchau - para sair   \n");
        printf("------------------------\n");
}
int main(int argc, char *argv[]) {


    struct sockaddr_in server;

    int sockfd;

    int len = sizeof(server);
    int slen;
    char buffer_in[TAM];

    char buffer_out[TAM];

    unsigned short int op;

    fprintf(stdout, "Starting Client ...\n");

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Error on client socket creation:");
        return EXIT_FAILURE;
    }
    fprintf(stdout, "Client socket created with fd: %d\n", sockfd);

    server.sin_family = AF_INET;
    server.sin_port = htons(PORTA);
    server.sin_addr.s_addr = inet_addr(SERVER_ADDR);
    memset(server.sin_zero, 0x0, 8);
    if (connect(sockfd, (struct sockaddr*) &server, len) == -1) {
        perror("Can't connect to server");
        return EXIT_FAILURE;
    }
    if ((slen = recv(sockfd, buffer_in, TAM, 0)) > 0) {
        buffer_in[slen + 1] = '\0';
        fprintf(stdout, "Server says: %s\n", buffer_in);
    }
    while (true) {
        memset(buffer_in, 0x0, TAM);
        memset(buffer_out, 0x0, TAM);
        
        
        mostrarmenu();
        fprintf(stdout, "Resposta: ");
        fgets(buffer_out, TAM, stdin);
        send(sockfd, buffer_out, strlen(buffer_out), 0);
        slen = recv(sockfd, buffer_in, TAM, 0);
        printf("Server answer: %s\n", buffer_in);
        if(strcmp(buffer_in, "tchau") == 0){
            break;
        }
        else if(strcmp(buffer_in, "nome") == 0){
            fprintf(stdout, "Digite o nome: ");
            fgets(buffer_out, TAM, stdin);
            send(sockfd, buffer_out, strlen(buffer_out), 0);
            
            memset(buffer_in, 0x0, TAM);
            memset(buffer_out, 0x0, TAM);
         
            slen = recv(sockfd, buffer_in, TAM, 0);
            printf("Server answer: %s\n", buffer_in);
            
            if (strcmp(buffer_in, "telefone") == 0){
                fprintf(stdout, "Digite o telefone: ");
                fgets(buffer_out, TAM, stdin);
                send(sockfd, buffer_out, strlen(buffer_out), 0);
            }
        }
         else if(strcmp(buffer_in, "como") == 0){
            mostrarmenu2();
            memset(buffer_in, 0x0, TAM);
            memset(buffer_out, 0x0, TAM);
            fprintf(stdout, "Como deseja procurar: ");
            fgets(buffer_out, TAM, stdin);
            send(sockfd, buffer_out, strlen(buffer_out), 0);
            
                slen = recv(sockfd, buffer_in, TAM, 0);
                printf("Server answer: %s\n", buffer_in);
                if(strcmp(buffer_in, "nome") == 0){
                    fprintf(stdout, "Digite o nome: ");
                    memset(buffer_out, 0x0, TAM);
                    fgets(buffer_out, TAM, stdin);
                    send(sockfd, buffer_out, strlen(buffer_out), 0);
                        do{
                            memset(buffer_in, 0x0, TAM);
                            slen = recv(sockfd, buffer_in, TAM, 0);
                            printf("%s", buffer_in);
                        }while(strcmp(buffer_in,"fim")!=0);
                    getchar();                  
                }
                else if(strcmp(buffer_in, "telefone") == 0){
                    fprintf(stdout, "Digite o telefone: ");
                    memset(buffer_out, 0x0, TAM);
                    fgets(buffer_out, TAM, stdin);
                    send(sockfd, buffer_out, strlen(buffer_out), 0);
                      do{
                            memset(buffer_in, 0x0, TAM);
                            slen = recv(sockfd, buffer_in, TAM, 0);
                            printf("\n%s\n", buffer_in);
                        }while(strcmp(buffer_in,"fim")!=0);
                    getchar();  
                }
        }
    }
    close(sockfd);
    fprintf(stdout, "\nConnection closed\n\n");

    return EXIT_SUCCESS;
}
