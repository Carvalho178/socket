#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define PORTA 5050
#define TMAX 70
#define TNOME 50
#define TTEL 17
typedef struct info_auxiliar{
    char info[50];
}auxiliar;
typedef struct Agenda_Telefonica{
	char anome[TNOME];
	char atel[TTEL];
} agendatel;
int contadorlinhas(){
    int contador=0;
	char info[50];
	FILE *fp;
	fp = fopen("SocketAgenda.txt", "r");
	if(fp == NULL)
			return 0;
	else
		while((fgets(info, sizeof(info), fp))!=NULL){
            contador++;
        }
	fclose(fp);
  return contador;
}
void inserircontato(char nome[TNOME], char telefone[TTEL]){
    unsigned short int tam;
    agendatel agenda;
    FILE *fop;
    tam = strlen(telefone);
    telefone[tam - 1] = '\0';
    tam = strlen(nome);
    nome[tam - 1] = '\0';
    fop = fopen("SocketAgenda.txt","a+");
    fprintf(fop, "%s\n%s\n", nome, telefone);
    fprintf(fop, "\n\n");
    fclose(fop);
}
int main(void) {

    struct sockaddr_in client, server;
    int serverfd, clientfd;
    char buffer[TMAX];
    char buffernome[TNOME];
    char buffertelefone[TTEL];
    char bufferpesquisa[TMAX];
    printf("Iniciando serivodr\n");

    /* Creates a IPv4 socket */
    serverfd = socket(AF_INET, SOCK_STREAM, 0);
    if(serverfd == -1) {
        perror("Can't create the server socket:");
        return EXIT_FAILURE;
    }
    fprintf(stdout, "Server socket created with fd: %d\n", serverfd);


    /* Defines the server socket properties */
    server.sin_family = AF_INET;
    server.sin_port = htons(PORTA);
    memset(server.sin_zero, 0x0, 8);


    /* Handle the error of the port already in use */
    int yes = 1;
    if(setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR,
                  &yes, sizeof(int)) == -1) {
        perror("Socket options error:");
        return EXIT_FAILURE;
    }


    /* bind the socket to a port */
    if(bind(serverfd, (struct sockaddr*)&server, sizeof(server)) == -1 ) {
        perror("Socket bind error:");
        return EXIT_FAILURE;
    }


    /* Starts to wait connections from clients */
    if(listen(serverfd, 1) == -1) {
        perror("Listen error:");
        return EXIT_FAILURE;
    }
    fprintf(stdout, "Listening on port %d\n", PORTA);

    socklen_t client_len = sizeof(client);
    if ((clientfd=accept(serverfd,
        (struct sockaddr *) &client, &client_len )) == -1) {
        perror("Accept error:");
        return EXIT_FAILURE;
    }


    /* Copies into buffer our welcome messaage */
    strcpy(buffer, "Hello, client!\n\0");


    /* Sends the message to the client */
    if (send(clientfd, buffer, strlen(buffer), 0)) {
        fprintf(stdout, "Client connected.\nWaiting for client message ...\n");

        /* Communicates with the client until bye message come */
        do {

            /* Zeroing buffers */
            memset(buffer, 0x0, TMAX);

            /* Receives client message */
            int message_len;
            if((message_len = recv(clientfd, buffer, TMAX, 0)) > 0) {
                buffer[message_len - 1] = '\0';
                printf("Client says: %s\n", buffer);
            }


            /* 'bye' message finishes the connection */
            if(strcmp(buffer, "tchau") == 0) {
             
                send(clientfd, "tchau", 5, 0);
            } else if (strcmp(buffer,"add")==0){
                memset(buffernome, 0x0, TNOME);
                memset(buffertelefone, 0x0, TTEL);
                
                send(clientfd, "nome", 4, 0);
                
                int t_n;
                if((t_n = recv(clientfd, buffernome, TNOME, 0)) > 0) {
                buffer[t_n - 1] = '\0';
                printf("Nome: %s\n", buffernome);
                
                    send(clientfd, "telefone", 8, 0);
                
                    int t_t;
                    if((t_t = recv(clientfd, buffertelefone, TTEL, 0)) > 0) {
                        buffer[t_n - 1] = '\0';
                        printf("Telefone: %s\n", buffertelefone);
                    }
                
                }
                
                inserircontato(buffernome, buffertelefone);
                
            }
            else if (strcmp(buffer,"prc")==0){
                send(clientfd, "como\n", 4, 0);
                
                memset(buffer, 0x0, TMAX);
                memset(buffernome, 0x0, TNOME);
                memset(buffertelefone, 0x0, TTEL);

                int message_len;
                if((message_len = recv(clientfd, buffer, TMAX, 0)) > 0) {
                    buffer[message_len - 1] = '\0';
                    printf("Client says: %s\n", buffer);
                }
                if (strcmp(buffer,"nome")==0){
                    send(clientfd, "nome\n", 4, 0);
                    int t_n;
                    
                    if((t_n = recv(clientfd, buffernome, TNOME, 0)) > 0) {
                        buffernome[t_n - 1] = '\0';
                        printf("Nome: %s\n", buffernome);
                    
                        if(contadorlinhas()){
                                int i; 
                                int tamanho;
                                int x=0;
                                unsigned short int ok = 0;
                                tamanho = contadorlinhas();
                                printf("%d\n", tamanho);
                                auxiliar aux[tamanho];
                                FILE*fp;
                                fp = fopen("SocketAgenda.txt", "r");
                                for (i=0;i<tamanho;i++){
                                    x = strlen(aux[i].info);
                                    aux[i].info[x-1] = '\0';
                                    fscanf(fp, "%s", aux[i].info);
                                }
                                fclose(fp);
                                for(i=0;i<tamanho;i++){
                                    printf("/buffer: %s /aux: %s\n", buffernome, aux[i].info);
                                    if(strcmp(buffernome, aux[i].info)==0){
                                        memset(bufferpesquisa, 0x0, TMAX);
                                        strcpy(bufferpesquisa,buffernome);
                                        strcat(bufferpesquisa," - ");
                                        strcat(bufferpesquisa,aux[i+1].info);
                                        send(clientfd, bufferpesquisa,strlen(bufferpesquisa),0);
                                    }
                                }
                            send(clientfd,"fim",3,0);
                        } 
                        else {
                            send(clientfd, "fim", 3, 0);
                        }
                    }
                }
                else if (strcmp(buffer,"telefone")==0){
                    send(clientfd, "telefone", 8, 0);
                
                    int t_t;
                    if((t_t = recv(clientfd, buffertelefone, TTEL, 0)) > 0) {
                        buffertelefone[t_t - 1] = '\0';
                        printf("Telefone: %s\n", buffertelefone);
                        
                        if(contadorlinhas()){
                                int i; 
                                int tamanho;
                                int x=0;
                                tamanho = contadorlinhas();
                                printf("%d\n", tamanho);
                                auxiliar aux[tamanho];
                                FILE*fp;
                                fp = fopen("SocketAgenda.txt", "r");
                                for (i=0;i<tamanho;i++){
                                    x = strlen(aux[i].info);
                                    aux[i].info[x-1] = '\0';
                                    fscanf(fp, "%s", aux[i].info);
                                }
                                fclose(fp);
                                for(i=0;i<tamanho;i++){
                                    printf("/buffer: %s /aux: %s\n", buffertelefone, aux[i].info);
                                    if(strcmp(buffertelefone, aux[i].info)==0){
                                        memset(bufferpesquisa, 0x0, TMAX);
                                        strcpy(bufferpesquisa,aux[i-1].info);
                                        strcat(bufferpesquisa," - ");
                                        strcat(bufferpesquisa,buffertelefone);
                                        send(clientfd, bufferpesquisa,strlen(bufferpesquisa),0);
                                    }
                                }
                            send(clientfd,"fim",3,0);
                        }
                      else{  
                        send(clientfd,"fim",3,0);
                      }
                    }
                }
            }
            else {
                send(clientfd, "opcao invalida\n", 15, 0);
            }

        } while(strcmp(buffer, "tchau"));
    }

    /* Client connection Close */
    close(clientfd);

    /* Close the local socket */
    close(serverfd);

    printf("Connection closed\n\n");

    return EXIT_SUCCESS;
}
